# ChessText

Get puzzles or review your openings as text. Learn Blindfold Chess.

## Installation

```
$ git clone git@gitlab.com:floum/chess_text.git
$ cd chess_text
$ gem build chess_text
$ gem install chess_text-*.gem
```

## Usage

```
$ chess-text puzzle
```

