# frozen_string_literal: true

require_relative "chess_text/version"
require_relative 'chess_text/puzzle'
require_relative 'chess_text/position'
require_relative 'chess_text/piece'

module ChessText
  class Error < StandardError; end
  # Your code goes here...
end
