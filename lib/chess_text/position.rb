module ChessText
  class Position
    attr_reader :pieces, :to_move

    def initialize(fen)
      @fen = fen
      @en_passant = nil
      @pieces = []
      @to_move = :white
      parse_fen
    end

    def inspect
      "#{to_move} to move.\n" <<
      "White: #{white_pieces}\n" <<
      "Black: #{black_pieces}\n"
    end

    private

    def white_pieces
      %w(K Q R B N P).map do |piece_type|
        pieces.select { |piece| piece_type == piece.type }.map(&:inspect)
      end.reject(&:empty?).join(' ')
    end

    def black_pieces
      %w(k q r b n p).map do |piece_type|
        pieces.select { |piece| piece_type == piece.type }.map(&:inspect)
      end.reject(&:empty?).join(' ')
    end

    def to_move
      @to_move == 'w' ? 'White' : 'Black'
    end

    def en_passant
      if @en_passant != '-'
        "En passant is available at #{@en_passant}."
      else
        "En passant is not available."
      end
    end

    def parse_fen
      position = @fen.split(' ').first
      position.split('/').each_with_index do |row, index|
        col_index = 0
        r = row
        until r.empty?
          if r[0].to_i == 0
            @pieces << Piece.new(r[0], square(col_index, index))
            col_index += 1
          else
            col_index += r[0].to_i
          end
          r = r[1..-1]
        end
      end
      @pieces.sort!

      @to_move = @fen.split(' ')[1]

      @en_passant = @fen.split(' ')[3]
    end

    def to_col(index)
      (index + 97).chr
    end

    def to_row(index)
      8 - index
    end

    def square(col_index, row_index)
      "#{to_col(col_index)}#{to_row(row_index)}"
    end

  end
end
