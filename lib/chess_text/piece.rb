module ChessText
  class Piece
    include Comparable

    attr_reader :type

    PIECE_TYPES = %w(K Q R B N P k q r b n p)

    def initialize(type, square)
      @type = type
      @square = square
    end

    def <=>(other)
      rank <=> other.rank
    end

    def rank
      PIECE_TYPES.index(@type)
    end

    def inspect
      "#{@type}#{@square}"
    end
  end
end
