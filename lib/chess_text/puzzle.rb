module ChessText
  class Puzzle
    attr_reader :expected_move, :position

    def initialize(input)
      @position = Position.new(input[1])
      @moves = input[2].split(' ')
      @moves_played = []
      @moves_played << @moves.shift
    end

    def self.fetch
      Puzzle.new(CSV.read('puzzles/puzzle_set.csv').sample)
    end

    def finished?
      @moves.empty?
    end

    def expected_move
      @moves.first
    end

    def advance
      @moves_played << @moves.shift unless finished?
      @moves_played << @moves.shift unless finished?
    end

    def last_move
      @moves_played.last
    end

    def inspect
      "#{@position.inspect}\nMoves: #{@moves_played}"
    end

  end
end
